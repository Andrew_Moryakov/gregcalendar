﻿using System;

namespace GregCalendar
{
	public sealed class GregCalendar
	{
		/// <summary>
		/// Возвращает количество дней в году
		/// </summary>
		/// <param name="year">Год</param>
		/// <returns>Возвращает количество дней в указанном году</returns>
		public int GetDayInYear(int year)
		{
			return IsLeapYear(year)? 366: 365;
		}

		/// <summary>
		/// Проверяет является ли указанный год високосным
		/// </summary>
		/// <param name="year">Год</param>
		/// <returns>Если год является високосным, то возвращает истину</returns>
		public Boolean IsLeapYear(int year)
		{
			Boolean isLeap;
			if (year % 4 != 0)
			{
				isLeap = false;
			}
			else if (year % 100 != 0)
			{
				isLeap = true;
			}
			else if (year % 400 != 0)
			{
				isLeap = false;
			}
			else
			{
				isLeap = true;
			}

			return isLeap;
		}

		/// <summary>
		/// Возвращает количество дней в месяце года
		/// </summary>
		/// <param name="month">Месяц</param>
		/// <param name="year">Год</param>
		/// <returns>Колечество дней</returns>
		public int GetDaysInMonth(int month, int year)
		{
			int days = 0;
			switch (month)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					days = 31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					days = 30;
					break;
				case 2:
					days = IsLeapYear(year)? 29: 28;
					break;
			}
			return days;
		}

		/// <summary>
		/// Возвращает название дня недели, указанного года месяца и дня
		/// </summary>
		/// <param name="day">День месяца</param>
		/// <param name="month">Месяц года</param>
		/// <param name="year">Год</param>
		/// <returns>Возвращает имя дня недели</returns>
		public WeekEnum.Days GetNameOfDayOfWeek(int day, int month, int year)
		{
			return (WeekEnum.Days)GetNumberDayOfWeek(day, month, year);
		}

		/// <summary>
		/// Возвращает номер дня недели, указанного года месяца и дня
		/// </summary>
		/// <param name="day">День месяца</param>
		/// <param name="month">Месяц года</param>
		/// <param name="year">Год</param>
		/// <returns>Номер дня недели</returns>
		public int GetNumberDayOfWeek(int day, int month, int year)
		{
			DateTime dt = new DateTime(year, month, day);
			return ((int)(dt.Ticks / 864000000000L) % 7);
		}

		/// <summary>
		/// Возвращает календарь н указанный месяц и год. Календарь представлен список из пяти недель.
		/// </summary>
		/// <param name="month">Месяц</param>
		/// <param name="year">Год</param>
		/// <returns>Возвращает список недель. Неделя представлена массивом из семи элементов. Каждый элемент в массиве содержит номер дня в месяце. Первый элемент каждого массива соответсвует понедельнику.</returns>
		public int[][] GetCalendarOfMonth(int month, int year)
		{
			#region variables

			int numDayOfWeek = GetNumberDayOfWeek(1, month, year);
			int lastDayOfMonth = GetDaysInMonth(month, year);
			int countLine = numDayOfWeek - 1;
			countLine += (7-GetNumberDayOfWeek(lastDayOfMonth, month, year));
			countLine += lastDayOfMonth;
			countLine /= 7;

			int[][] calendar = new int[countLine][];
			lastDayOfMonth = GetDaysInMonth(month, year);
			const int countDaysOfWeek = 7;
			int numOfWeek = 0;

			int[] week = new int[countDaysOfWeek];
			#endregion

			for (int day = 1; day <= lastDayOfMonth; day++)
			{
				numDayOfWeek = GetNumberDayOfWeek(day, month, year);
				week[numDayOfWeek] = day;

				if (numDayOfWeek == (int)WeekEnum.Days.Sun || day == lastDayOfMonth)
				{
					calendar[numOfWeek] = week;
					week = new int[countDaysOfWeek];
					numOfWeek++;
				}

			} 

			return calendar;
		}

		public string FormatForPrint(int[][] calendar, int currentDay)
		{
			#region variables
			char divider = '.';
			int step = 5;
			int lengthEmptyLine = step * calendar.Length;
			string calendarLine="";// = new string(divider, lengthEmptyLine);
			int countWeeks = calendar.Length;
			int countDayInWeek = 7;
			int posForReplace = 0;
			string formattedCalendar = "";

			FormattingCalendar formatter = new FormattingCalendar(divider, step);
			#endregion

			for (int day = 0; day < countDayInWeek; day++)
			{
				for (int week = 0; week < countWeeks; week++)
				{//Проходим по вершинам массива. На каждой итерации извлекаем числа который попадают на один и тот же день
					posForReplace += step;
					calendarLine += formatter.ReplaceTheDividerByNumber(calendarLine, calendar[week][day], posForReplace, currentDay);
				}

				formattedCalendar += formatter.AddLineToFullCalendar(day, calendarLine);
				posForReplace = 0;
				calendarLine = "";//new string(divider, lengthEmptyLine);
			}
			return formattedCalendar;
		}
	}
}