﻿using System;
using System.Linq;
using System.Text;

namespace GregCalendar
{
	internal sealed class FormattingCalendar
	{
		private const string _format = "[{0}{1}]";
		private readonly char _divider;
		private readonly int _step;

		internal FormattingCalendar(char divider, int step)
		{
			_divider = divider;
			_step = step;
		}

		/// <summary>
		/// Добавляет число месяца к исходную строку
		/// </summary>
		/// <param name="day"></param>
		/// <param name="calendarLine"></param>
		/// <returns></returns>
		internal string AddLineToFullCalendar(int day, string calendarLine)
		{
			string nameOfDay = Enum.GetName(typeof(WeekEnum.Days), day);
			calendarLine += Environment.NewLine;
			calendarLine = calendarLine.Remove(0, 1);

			return $"{nameOfDay}{calendarLine}";
        }

		internal string ReplaceTheDividerByNumber(string orign, int newValue, int replacePos, int currentDay)
		{
			StringBuilder newString = new StringBuilder(newValue.ToString());
			StringBuilder space = new StringBuilder(new string(_divider, _step));

			if (currentDay == newValue)
			{
				if (orign.Any())
				{
					newString.Clear();
					newString.Append(string.Format(_format, _divider, newString));
				}
				else
				{
					newString.Clear();
					newString.Append(string.Format(_format, newValue.ToString(), ""));
				}
			}

			for (int i = 0; i < newString.Length; i++)
			{
				int indexForRemove = space.Length - (i+1);
				space.Remove(indexForRemove, 1);
				space.Append(newString[i]);
			}

			return space.ToString();
		}
	}
}