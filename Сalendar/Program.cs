﻿using System;
using System.Collections.Generic;
using System.Text;
using GregCalendar = GregCalendar.GregCalendar;

namespace Сalendar
{
	public sealed class Gs
	{
		public string[] Days{ get; } = {
			"mon",
			"tue",
			"wed",
			"thu",
			"fri",
			"sat",
			"sun"
		};

		private Dictionary<int, int[]> GetDayOfMonth(DateTime currentDate)
		{
			return null;
		}

		public Boolean IsLeapYear(int year)
		{
			Boolean leap;
			if (year % 4 != 0)
			{
				leap = false;
			}
			else if (year % 100 != 0)
			{
				leap = true;
			}
			else if (year % 400 != 0)
			{
				leap = false;
			}
			else
			{
				leap = true;
			}


			return leap;
		}

		public int GetDayInYear(int year)
		{
			return IsLeapYear(year)? 366: 365;
		}

		public int GetDayInMonth(int month, int year)
		{
			int days = 0;
			switch (month)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					days = 31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					days = 30;
					break;
				case 2:
					days = IsLeapYear(year)? 29: 28;
					break;
			}
			return days;
		}

		private int NumberOfDayOfWeek(int day, int month, int year)
		{
			int a = (14 - month) / 12, y = year - a, m = month + 12 * a - 2;
			return ((7006 + (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12)) % 7);
		}

		public string GetNameDayOfWeek(int day, int month, int year)
		{
			return Days[NumberOfDayOfWeek(day, month, year)];
		}

		public int GetDayOfWeek(int day, int month, int year)
		{
			return NumberOfDayOfWeek(day, month, year);
		}

		public List<int[]> GetDaysOfMonth(int month, int year)
		{
			List<int[]> result = new List<int[]>();
			int daysInMonth = GetDayInMonth(month, year);
			const int numWeeksOfMonth = 5;
			const int numDaysOfWeek = 7;

			int[] daysOfWeek = new int[numDaysOfWeek];
			for (int day = 0; day <= daysInMonth+1; day++)
			{
				int numOfDayOfWeek = (day) % numDaysOfWeek;

				if ((numOfDayOfWeek == 0 || day==daysInMonth+1) && day>0)
				{
					result.Add(daysOfWeek);
					daysOfWeek = new int[numDaysOfWeek];
				}

				daysOfWeek[numOfDayOfWeek] = day;
			}
			return result;
		}
	}

	class Program
	{
		private static string ForPrint(int y, int d, int m)
		{
			//StringBuilder twentyFiveDots = new StringBuilder(".........................");
			//string dots = new StringBuilder(twentyFiveDots.ToString()).ToString(); ;
			//const int countWeeks = 5;
			//int numOfWeek = 0;
			//int pointForReplace = 0;
			//int numOfDayOfWeek = 0;
			//string result = "";

			//for (int day = 0; day <= 32; day++)
			//{
			//	numOfWeek = (day) % countWeeks;
			//	if (numOfWeek == 0 && day > 0)
			//	{
			//		string nameOfDay = Enum.GetName(typeof(WeekEnum.Days), numOfDayOfWeek);
			//		dots = $"{dots}{Environment.NewLine}";
			//		dots = dots.Remove(0, 1);
			//		result += $"{nameOfDay}{dots}";

			//		pointForReplace = 0;
			//		dots = new StringBuilder(twentyFiveDots.ToString()).ToString();
			//		numOfDayOfWeek++;
			//	}
			//	pointForReplace += 5;

			//	//dots.Replace(".", calendar[numOfWeek][numOfDayOfWeek].ToString(), pointForReplace, 1);
			//	dots = ReplaceCharToString(dots, calendar[numOfWeek][numOfDayOfWeek], pointForReplace);
			//}
			//return result;



			//GregCalendar c = new GregCalendar();
			//string lineResult = "";
			//string dots = "...";
			//string day;
			//var r = c.GetCalendarOfMonth(m, y);
			//for (int i = 0; i < 7; i++)
			//{
			//	lineResult += c.Days[i];
			//	//Console.Write(c.Days[i]);
			//	for (int j = 0; j < 5; j++)
			//	{
			//		dots = "   ";
			//		day = r[j][i].ToString();
			//		day = day == "0"? " ": day;
			//		if (day == d.ToString())
			//		{
			//			if (j == 0)
			//			{
			//				day = "[ " + day + "]";
			//				dots = " ";
			//			}
			//			else
			//			{
			//				day = "[" + day + "]";
			//			}

			//		}
			//		if (j > 0)
			//		{

			//			if (day.Length == 1)
			//			{
			//				dots = "    ";
			//			}
			//			else if (day.Length > 2)
			//			{
			//				dots = "  ";
			//			}
			//			if (r[j - 1][i] == d)
			//			{
			//				dots = dots.Remove(dots.Length - 1);
			//			}

			//		}
			//		lineResult += dots + day;
			//		// Console.Write(dots + day);
			//	}
			//	lineResult += Environment.NewLine;
			//}

			//return lineResult;
			return "";
		}

		

		static void Main(string[] args)
		{ 
			global::GregCalendar.GregCalendar gec = new global::GregCalendar.GregCalendar();
			var r = gec.GetCalendarOfMonth(5, 2016);
			Console.WriteLine(gec.FormatForPrint(gec.GetCalendarOfMonth(5, 2016), 1));
			Console.ReadLine();
		}
	}
}